= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [0.5.0] - 2021-07-20

- Fix broken demo page by forcing placehold.it image urls to be http, since they still don't support https.

== [0.4.0] - 2021-05-26

- update dependencies. Now requires node >= 8 to build.

== [0.3.0] – 2019-10-04

- complemented usage documentation and simplified scss import
- fixed lesser error in scss

== [0.2.0] – 2019-10-04

npmjs ready version

== [0.1.0] – 2019-10-04

Initial working version



